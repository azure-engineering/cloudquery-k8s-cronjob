resource "kubernetes_namespace" "cloudquery_namespace" {
  metadata {
    name = "cloudquery"
  }
}

resource "kubernetes_config_map" "cloudquery_config" {
  metadata {
    name = "cloudquery-config"
  }

  data = {
    "config.tpl.yaml" = templatefile("cq.config.tpl", {})
  }
}


resource "kubernetes_cron_job" "cloudquery_cron" {
  metadata {
    name = "cloudquery-cron"
  }

  spec {
    schedule = "0 * * * *"

    job_template {
      metadata {
        name = "cloudquery-job"
      }

      spec {
        template {
          metadata {
            name = "cloudquery-pod"
          }

          spec {
            container {
              name    = "cloudquery"
              image   = "cloudquery/cloudquery"
              command = ["sync", "/config.tpl.yml"]

              volume_mount {
                name       = "cloudquery-config"
                mount_path = "/config.tpl.yml"
              }
            }

            volume {
              name = "cloudquery-config"

              config_map {
                name = "cloudquery-config"
              }
            }

            restart_policy = "Never"
          }
        }

        backoff_limit = 0
        completions   = 1
        parallelism   = 1
      }
    }
  }

  depends_on = [
    kubernetes_config_map.cloudquery_config
  ]
}
