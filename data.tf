data "azurerm_kubernetes_cluster" "k8s_cluster" {
  name                = var.cluster-name
  resource_group_name = var.cluster-rg
}